import {PlaceData} from './PlaceData'
import { combineReducers, createStore, applyMiddleware } from "redux"
import thunk from 'redux-thunk'
const place = new PlaceData()
console.log('bees',place.store)

const store = createStore(
	combineReducers({
		placeStore: place.reducer
	}),
	{
		placeStore: place.store
	},
	applyMiddleware(thunk)
)

export const placeData = new PlaceData()
export {store}