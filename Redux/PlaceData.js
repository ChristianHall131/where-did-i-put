import AsyncStorage from '@react-native-async-storage/async-storage';
import { combineReducers } from 'redux';
export class PlaceData {
  t = {
    GET_DATA: 'GET_DATA',
    SET_ITEMS: 'SET_ITEMS',
    SET_PLACES: 'SET_PLACES',
    ADD_ITEMS: `ADD_ITEMS`,
    ADD_PLACES: `ADD_PLACES`

  }
  actions = {
    getData: () => ({type: this.t.GET_DATA, payload:{data}}),
    addItems: items => ({type: this.t.ADD_ITEMS, payload: {items}}),
    setPlaces: places => ({type: this.t.ADD_PLACES, payload: {places}}),
    setItems: items => ({type: this.t.SET_ITEMS, payload: {data}}),
  }
  store = {
    items: [],
    places: [],
  };
  reducer = (store = this.store, action) => {
    switch (action.type) {
      case this.t.GET_DATA: {
        console.log("a",action.payload)
        return {...action.payload.data};
      }
      case this.t.ADD_ITEMS: {
        console.log('l',action.payload)
        return {...store,places:action.payload.items};
      }
      case this.t.SET_ITEMS: {
        //pass in an array of items
        console.log('chee')
        const places = store.places;
        items = action.payload.items
        // await AsyncStorage.setItem(`items`,JSON.stringify(items))
        return {items, places};
      }
      case this.t.ADD_PLACES: {
        //pass in an array of places
        console.log('eebs')
        const items = store.items;
        const places = store.places;
        places = [...places, ...action.payload.places];
        // await AsyncStorage.setItem(`places`,JSON.stringify(places))
        return {items, places};
      }
      default: {
        return store;
      }
    }
  }
  getData = () => async(dispatch) =>{
    let places = [];
    let items = [];
    try{
    let storedPlaces = await AsyncStorage.getItem('places');
    storedPlaces = JSON.parse(storedPlaces)
    places = storedPlaces?storedPlaces:[]
    console.log('b',places)
    } catch(e){
      console.warn('yee',e)
      await AsyncStorage.setItem('places',JSON.stringify([]))
    }
    try{
      let storedItems =  await AsyncStorage.getItem('items');
      storedItems = JSON.parse(storedItems)
      items = storedItems?storedItems:[]
      console.log('v',items)
    }catch(e){
      console.warn(e)
      await AsyncStorage.setItem('items',JSON.stringify([]))

    }
    console.log('h',items,places)
    console.log(`wat`)
    dispatch(this.actions.getData(data={items,places}))
  }
  addItem = (newItem) => async(dispatch,getState) =>{
    console.log(newItem)
    console.log(`wat1`)
    const items = getState().placeStore["items"];
    console.log('poooo',items)
    const itemsArray = [...items, newItem];
    console.log(itemsArray)
    dispatch(this.actions.addItems([...itemsArray]))
    await AsyncStorage.setItem(`items`,JSON.stringify(itemsArray))
  }
  addItems = (newItems) => (dispatch) => {
    console.log(`wat2`)
    dispatch(this.actions.addItems([...newItems]))
  }
  addPlace = (place) => (dispatch) =>{
    dispatch(this.actions.setPlaces([place]))
    console.log(`wat3`)
  }
  updateItems = (items) => (dispatch)=>{
    dispatch(this.actions.setItems(items))
    console.log(`wat4`)
  }
}
