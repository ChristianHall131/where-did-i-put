/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native'
import { Provider } from 'react-redux'
import {store} from './Redux'
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import MainScreen from './Screens/Main'

console.log(MainScreen)

const App: () => Node = () => {

  return (
    <Provider store={store}>
      <Text>AYEEEEE</Text>
      <MainScreen/>
    </Provider>
  );
};

export default App;
