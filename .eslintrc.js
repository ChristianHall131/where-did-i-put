module.exports = {
  root: true,
  extends:"react-app",
  rules: {
    indent: ['warn', 2],
    // we want to avoid extraneous spaces
  },
};
