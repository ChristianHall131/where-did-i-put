import React,{useEffect, useState,useRef} from 'react'
import { placeData } from '../../Redux';
import { connect } from 'react-redux';
import SelectDropdown from 'react-native-select-dropdown';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    AsyncStorage,
    FlatList,
    Alert,
    TextInput,
    Modal,
    Button,
    Pressable
  } from 'react-native';

const MainScreen = props => {

    const {
        places,
        items,
        addItem,
        addPlace,
        navigation,
        getData,
        updateItems
    } = props
    console.log('items',items)
    const [editIndex,setEditIndex] = useState(-1)
    const [editText,setEditText] = useState('')
    const [editPlace,setEditPlace] = useState('')
    // const [placesList, setPlacesList] = useState(newPlaces?newPlaces.push('Add New Place +'):['Add New Place+'])
    const [placesList, setPlacesList] = useState([])
    const [addingItem,setAddingItem] = useState(false)
    const [modalOpen,setModalOpen] = useState(false)
    const [allItems,setAllItems] = useState([])
    const [allPlaces,setAllPlaces] = useState([])
    const placeRef = useRef()

    useEffect(()=>{
        getData()
        const newPlaces = places? [...places]:[]
        newPlaces.push(`Add New Place +`)
        setPlacesList(newPlaces)
    },[])
    useEffect(()=>{
        setAllItems(items)
        setAllPlaces(places)
    })
    const addNewItem = () =>{
        const itemName = editText
        const itemPlace = editPlace
        setAddingItem(false)
        setEditText('')
        setEditPlace('')
        const item = {
            name: itemName,
            place: itemPlace,
        }
        addItem(item)
    }
    const handleChangePlace = (place) =>{
        if(editIndex !== -1){
            console.log('meep')
        }
        else{
            setEditPlace(place)
        }
    }
    const handleEditTitle = () =>{
        const title = editText
        const item = items[editIndex]
        const newItems = items
        item.title = title
        newItems[editIndex] = item
        updateItems(newItems)
    }
    const handleEditPlace = (place,index) =>{
        const item = items[index]
        const newItems = items
        item.place = place
        newItems[editIndex] = item
        updateItems(newItems)
    }
    const handleAddPlace = () =>{
        console.log(placeRef.current)
    }
    return(
        <View style={{height:'100%',width:'100%'}}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalOpen}
                onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalOpen(!modalOpen);
                }}
            >
                <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <SelectDropdown
                        data={places}
                        onSelection={(selection)=>handleChangePlace(selection)}
                    />
                    <TextInput ref={placeRef}/>
                    <Button
                        title={'Add New Place'}
                        onPress={handleAddPlace}
                    />
                    <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => setModalOpen(!modalOpen)}
                    >
                    <Text style={styles.textStyle}>Close</Text>
                    </Pressable>
                </View>
                </View>
            </Modal>
            <FlatList
                data={allItems}
                renderItem = {
                    ({item, index}) => (
                        <View style ={{flexDirection:`row`}}>
                            {editIndex === index?(
                                <>
                                <TextInput
                                    value={editText}
                                    placeHolder={'Name'}
                                    onChangeText = {setEditText}
                                />
                                <Button title={save} onPress={handleEditTitle}/>
                                </>
                            ):(
                                <Text>{item.name}</Text>
                            )}
                            <Button title={item.place} onPress={()=>setModalOpen(true)}/>
                        </View>
                    )
                }
            />
            {/* <View style ={{flexDirection:'row'}}>
                {editIndex === index?(

                    <TextInput
                        value={editText}
                        placeHolder={'Name'}
                        onChangeText = {setEditText}
                    />

                ):(
                    <Text>{item.name}</Text>
                )}
                <Button title={item.place}/>
            </View> */}
            {!addingItem?(
                <Button style={{height:'100px',width:'100px'}} title={`Add New Item`} onPress={()=>setAddingItem(true)}/>
            ):(
                <View style ={{flexDirection:'row'}}>
                    <TextInput
                        value={editText}
                        placeHolder={'Name'}
                        onChangeText = {e => setEditText(e)}
                />
                <Button title={editPlace?editPlace:'Select Place'}/>
                <Button
                    title={'Save'}
                    onPress={addNewItem}
                />
            </View>
            )}
            
        </View>
    )
}

const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
  });

const mapSateToProps = ({placeStore}) =>({
    items: placeStore.items,
    places: placeStore.places,
})
const mapDispatchToProps = {
    addItem: placeData.addItem,
    addPlace: placeData.addPlace,
    getData: placeData.getData,
    updateItems: placeData.updateItems,
}
export default connect(mapSateToProps,mapDispatchToProps)(MainScreen)